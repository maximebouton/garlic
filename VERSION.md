- [0.13] auto tempo corrector for manual oscillator/organ trigger and tuning
- [0.12] manual oscillator trigger and tuning
- [0.11] add triangle to defaults osc type, clear button,
- [0.10] new font (Donjon)
- [0.09] toggle copy_setup only when focus is off
- [0.08] selecting an organ should add it to organs selector
- [0.07] tempo should have an "measure" parameter
- [0.06] duration should have an "offset" parameter
- [0.05] tempo should update next tempo occurence
- [0.04] import / export 
- [0.03] separate git for samples sources 
- [0.02] organ should not be modifiable "one by one" 
- [0.01] select a organ should copy its content to controls


v1.0 (ordered by priority)
--------------------------

- IMPORTANT : lors de l'entrée en manual mode toutes les notes jouent ! ( ??? )
- Clear all but not selected button
- Change selected organs for manual trigger and tunning during manual mode
- onglets ( F or Fn commands to access saved setups )
- phase or start delay
- duration effect (ex : duration / 4 ) on manual play  
- manual recording ?
- samples sometime dont stop after clear or sample changement
- trigger type options
- delay trigger type option
- command line bonus interface for variables and such
- correspondant placeholder
- tempo should be validated and reversible
- wave or sample
- Documentation
- website
- Devlog
- tempo crash ( find and fix it )
- imported organs (with sample ?) do not work correctly
- bind interfaces values to input eval (this = parameter value)
- inside help / indications / tips
- complete notes variables
- parameters should have clear and valid defaults
- when invalid, parameters should not give some improper output
- express error / invalid value (red ?)
- display calculated values
- error log
- eval import ?
- sample buffer load is buggy
- order functions are buggy
- tempo measure and duration offset are not very understanable
- Pitch events
- Simplifiying organs parameters
- "create simplified langage" to call pre-written functions
- import "offset" parameter ? _dont remember what that mean_
- review and clean code
- too slow on old computer ( buffer + oscillator per organ ? ) and bad sound cards ?
- separated scripts


v2.0
----

- better interface ( full textmode ? )
- rooms
- synchronised base tempo between rooms
- multi-player rooms
- terminal version 
