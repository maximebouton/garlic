# Garlic

Generating audio and record-sampling for live improvisation and composition.


## Introduction

Garlic is a multi-player web application mostly written in javascript, based on the web audio api and nodeJs.
The way it meant te be used is strongly inspired by text-based user interface interactions and live-coding.
The basic principle is to configure 10 musical organs being individually both oscillators and samplers.
This configuration is done by writing short lines of code calling up pre-written functions.


## Organs

Each organ is configurable throught 4 parameters.

- Quality
- Duration
- Pitch events
- Gain events


### Quality

Quality determine if the organ is used as a sampler or a synthetizer.
It can handle 3 different types of input.


#### String

Wich can call basic Web audio API oscillator types :

- "sine"
- "triangle"
- "square"
- "sawtooth"

Or determine the source file path of the sampler

- "path/to/file"


#### Array 

Wich can define a custom waveform for the oscillator

- [ float, float, … ]

_Currently in progress both in term of use and conception_


### Duration


### Pitch Events


### Gain Events


## Pre-written functions


### Random


#### Integer


#### Float


#### Array


### Order


#### Number


#### Array


## Interface

### Shortcuts

### Activation

### Tempo

### Import / Export
