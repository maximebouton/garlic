// # sounds

s = {}

// # notes

// C0	        16.35 	
// C#0/Db0  	17.32 	
// D0	        18.35 	
// D#0/Eb0  	19.45 	
// E0	        20.60 	
// F0	        21.83 	
// F#0/Gb0  	23.12 	
// G0	        24.50 	
// G#0/Ab0  	25.96 	
// A0	        27.50 	
// A#0/Bb0  	29.14 	
// B0	        30.87 	
// C1	        32.70 	
// C#1/Db1  	34.65 	
// D1	        36.71 	
// D#1/Eb1  	38.89 	
// E1	        41.20 	
// F1	        43.65 	
// F#1/Gb1  	46.25 	
// G1	        49.00 	
// G#1/Ab1  	51.91 	
// A1	        55.00 	
// A#1/Bb1  	58.27 	
// B1	        61.74 	
// C2	        65.41 	
// C#2/Db2  	69.30 	
// D2	        73.42 	
// D#2/Eb2  	77.78 	
// E2	        82.41 	
// F2	        87.31 	
// F#2/Gb2  	92.50 	
// G2	        98.00 	
// G#2/Ab2  	103.83 	
// A2	        110.00 	
// A#2/Bb2  	116.54 	
// B2	        123.47 	
// C3	        130.81 	
// C#3/Db3  	138.59 	
// D3	        146.83 	
// D#3/Eb3  	155.56 	
// E3	        164.81 	
// F3	        174.61 	
// F#3/Gb3  	185.00 	
// G3	        196.00 	
// G#3/Ab3  	207.65 	
// A3	        220.00 	
// A#3/Bb3  	233.08 	
// B3	        246.94 	
// C4	        261.63 	
// C#4/Db4  	277.18 	
// D4	        293.66 	
// D#4/Eb4  	311.13 	
// E4	        329.63 	
// F4	        349.23 	
// F#4/Gb4  	369.99 	
// G4	        392.00 	
// G#4/Ab4  	415.30 	
// A4	        440.00 	
// A#4/Bb4  	466.16 	
// B4	        493.88 	
// C5	        523.25 	
// C#5/Db5  	554.37 	
// D5	        587.33 	
// D#5/Eb5  	622.25 	
// E5	        659.25 	
// F5	        698.46 	
// F#5/Gb5  	739.99 	
// G5	        783.99 	
// G#5/Ab5  	830.61 	
// A5	        880.00 	
// A#5/Bb5  	932.33 	
// B5	        987.77 	
// C6	        1046.50 
// C#6/Db6  	1108.73 
// D6	        1174.66 
// D#6/Eb6  	1244.51 
// E6	        1318.51 
// F6	        1396.91 
// F#6/Gb6  	1479.98 
// G6	        1567.98 
// G#6/Ab6  	1661.22 
// A6	        1760.00 
// A#6/Bb6  	1864.66 
// B6	        1975.53 
// C7	        2093.00 
// C#7/Db7  	2217.46 
// D7	        2349.32 
// D#7/Eb7  	2489.02 
// E7	        2637.02 
// F7	        2793.83 
// F#7/Gb7  	2959.96 
// G7	        3135.96 
// G#7/Ab7  	3322.44 
// A7	        3520.00 
// A#7/Bb7  	3729.31 
// B7	        3951.07 
// C8	        4186.01 
// C#8/Db8  	4434.92 
// D8	        4698.63 
// D#8/Eb8  	4978.03 
// E8	        5274.04 
// F8	        5587.65 
// F#8/Gb8  	5919.91 
// G8	        6271.93 
// G#8/Ab8  	6644.88 
// A8	        7040.00 
// A#8/Bb8  	7458.62 
// B8	        7902.13 

var C = [16.35,32.70,65.41,130.81,261.63,523.25,1046.50,2093.00,4186.01]

for ( var i = 0 ; i < C.length ; i++ ){
    window['C'+i] = C[i]
}

var reb = [17.32,34.65,69.30,138.59,277.18,554.37,1108.73,2217.46,4434.92]
var D = [18.35,36.71,73.42,146.83,293.66,587.33,1174.66,2349.32,4698.63]

for ( var i = 0 ; i < D.length ; i++ ){
    window['D'+i] = D[i]
}

var mib = [19.45,38.89,77.78,155.56,311.13,622.25,1244.51,2489.02,4978.03]
var E = [20.60,41.20,82.41,164.81,329.63,659.25,1318.51,2637.02,5274.04]
for ( var i = 0 ; i < E.length ; i++ ){
    window['E'+i] = E[i]
}
var F = [21.83,43.65,87.31,174.61,349.23,698.46,1396.91,2793.83,5587.65]
for ( var i = 0 ; i < F.length ; i++ ){
    window['F'+i] = F[i]
}
var solb = [23.12,46.25,92.50,185.00,369.99,739.99,1479.98,2959.96,5919.91]
var G = [24.50,49.00,98.00,196.00,392.00,783.99,1567.98,3135.96,6271.93]
for ( var i = 0 ; i < G.length ; i++ ){
    window['G'+i] = G[i]
}
var lab = [25.96,51.91,103.83,207.65,415.30,830.61,1661.22,3322.44,6644.88 ]
var A = [27.50,55.00,110.00,220.00,440.00,880.00,1760.00,3520.00,7040.00]
for ( var i = 0 ; i < A.length ; i++ ){
    window['A'+i] = A[i]
}
var B = [30.87,61.74,123.47,246.94,493.88,987.77,1975.53,3951.07,7902.13]
for ( var i = 0 ; i < B.length ; i++ ){
    window['B'+i] = B[i]
}
// # tones

tones = [5.946,12.246,18.921,25.992,33.484,41.421,49.831,58.740,68.179,78.180,88.775,100];

// # create

c = {}

// ## create array

c.a = function(value,start,end,progression){
    var arr = []
    for ( var i = start; i < end; i += progression ){
        arr.push(value/i)
    }
    return arr
}

// create imag for custom waveform (temporary)

c.i = function(definition,base_array){
    var arr = base_array
    for ( var i = 0; i < definition; i++ ){
        arr = arr.concat(base_array)
    }
    return arr
}

// # order

o = {}

o.arr = {}

o.a = function(id, arr, progression, reverse){
    if (o.arr[id] == undefined) o.arr[id] = [ 0, 1 ]
    if ( isNaN(o.arr[id][0]) ) o.arr[id][0] = 0

    var to_return = arr[parseInt(o.arr[id][0])]
    o.arr[id][0] += progression * o.arr[id][1]
    if ( !reverse ) {
        if ( o.arr[id][1] != progression ) o.arr[id][1] = progression
        if ( o.arr[id][0] <= 0 ) o.arr[id][0] = arr.length
        if ( o.arr[id][0] >= arr.length ) o.arr[id][0] = 0
    } else {
        if ( o.arr[id][0] < 0 ) {
            o.arr[id][1] *= -1
            o.arr[id][0] = 0
            o.arr[id][0] += progression * o.arr[id][1]
        }
        if ( o.arr[id][0] >= arr.length-1 ) {
            o.arr[id][1] *= -1
            o.arr[id][0] = arr.length
            o.arr[id][0] += progression * o.arr[id][1]
        }
    }
    return to_return
}

o.num = {}

o.n = function(id, base, limite, progression, reverse){
    if (o.num[id] == undefined) o.num[id] = [ base, 1 ]
    if ( isNaN(o.num[id][0]) ) o.num[id][0] = base
    var to_return = o.num[id][0]
    o.num[id][0] += progression * o.num[id][1]
    if ( !reverse ) {
        if ( o.num[id][0] <= base )          o.num[id][0] = limite
        if ( o.num[id][0] >= limite )       o.num[id][0] = base
    } else {
        if ( o.num[id][0] < base ) {
            o.num[id][1] *= -1
            o.num[id][0] = base
            o.num[id][0] += progression * o.num[id][1]
        }
        if ( o.num[id][0] > limite ) {
            o.num[id][1] *= -1
            o.num[id][0] = limite
            o.num[id][0] += progression * o.num[id][1]
        }
    }
    return to_return
}

// # rdm
r = {};

// ## int
r.i = function(min, max) {
    return parseInt(Math.random() * (max - min) + min);
}

// ## float
r.f = function(min, max) {
    return Math.random() * (max - min) + min;
}

// ## array
r.a = function(arr) {
    var n = r.i(0,arr.length);
    return arr[n];
}

// ## object
r.o = function(obj) {
    length=0; for(i in obj)length++
    var n = r.i(0,length);
    index=0; for(i in obj){index++;if(index==n){return i}}
}

// ## dimension
r.d = function() {
    var l = r.a(carets[carets.length-1].dimension.context.context.content);
    return r.a(l.content)
}

// ## point
r.p = function(n, min, max) {
	var pts=[]
    for(i=0;i<n;i++)pts.push([r.i(min,max),r.i(min,max)])
    return pts
}

// ## characters
r.l = function(n,chars) {
    var str=""
    for(i=0;i<n;i++)str+=chars[r.i(0,chars.length)]
    return str  
}
