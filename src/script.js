// init edit inputs

var boot_values = [
  "\"\"",
  "\"\"",
  "[1/4,4]",
  "[0]",
  "\"sine\"",
  "\"src/\"",
  "[[0,0.01]]",
  "[[0,0.01],[0,0.01,0]]",
  "[1,Infinity]",
  "1"
]

var inputs = document.getElementById("controls").getElementsByTagName("input");
for( var i = 0 ; i < inputs.length ; i++ ){
  inputs[i].value = boot_values[i];
}; 

// Solo mode

var solo_mode = false;

var solo_freq_input = document.getElementById("solo_freq").getElementsByTagName("input")[0];
solo_freq_input.value = "[[X,0.01]]";

var notes_input = document.getElementById("notes").getElementsByTagName("input")[0];
notes_input.value = '{"w":"A3","x":"B3","c":"C3","v":"D3","b":"E3","n":"F3","q":"G3","s":"A4","d":"B4","f":"C4","g":"D4","h":"E4","j":"F4","k":"G4","l":"A5","m":"B5","a":"C5","z":"D5","e":"E5","r":"F5","t":"G5","y":"A6","u":"B6","i":"C6","o":"D6","p":"E6"}';
notes = {
  "w":"A3",
  "x":"B3",
  "c":"C3",
  "v":"D3",
  "b":"E3",
  "n":"F3",
  "q":"G3",
  "s":"A4",
  "d":"B4",
  "f":"C4",
  "g":"D4",
  "h":"E4",
  "j":"F4",
  "k":"G4",
  "l":"A5",
  "m":"B5",
  "a":"C5",
  "z":"D5",
  "e":"E5",
  "r":"F5",
  "t":"G5",
  "y":"A6",
  "u":"B6",
  "i":"C6",
  "o":"D6",
  "p":"E6"
};


// clean organ inputs

var organs = document.getElementsByClassName("organ");
for ( var j = 0 ; j < organs.length ; j++ ) {
  var inputs = organs[j].getElementsByTagName("input");
  for( var i = 0 ; i < inputs.length ; i++ ){
    inputs[i].value = '';
  };
};


// --- Keyboard Naviguation ---

var context_on = undefined
var focus_on = undefined

var keys = {}
var released_keys = {}

document.addEventListener("keydown",function(e){
    // e.preventDefault();e.stopPropagation();
    let key = e.key.toLowerCase()
    keys[key] = true;
    if ( solo_mode && e.key != 'K') return
    if ( e.key == 'Escape' ) {
        if ( !focus_on ) copy_setup(-1)
        var to_escape = focus_on
        focus_on = undefined
        focus(to_escape)
    }

    // focus

    if ( e.key == 'i' ) focus("import")
    if ( e.key == 'e' ) focus("export")
    if ( e.key == 't' ) focus("tempo")
    if ( e.key == 'o' ) focus("organs")
    if ( e.key == 'w' ) focus("waveform")
    if ( e.key == 'f' ) focus("frequency")
    if ( e.key == 'g' ) focus("gain")
    if ( e.key == 'd' ) focus("duration")
    if ( e.key == 's' ) focus("sample")
    if ( e.key == 'p' ) focus("pitch")
    if ( e.key == 'l' ) focus("location")
    if ( e.key == 'm' ) focus("solo_freq")
    if ( e.key == 'n' ) focus("notes")
    
    // activate

    if ( e.key == 'T' ) activate("tempo")
    if ( e.key == 'A' ) activate("apply")
    if ( e.key == 'B' ) activate("undo")
    if ( e.key == 'C' ) activate("clear")
    if ( e.key == 'W' ) activate("waveform")
    if ( e.key == 'F' ) activate("frequency")
    if ( e.key == 'G' ) activate("gain")
    if ( e.key == 'D' ) activate("duration")
    if ( e.key == 'S' ) activate("sample")
    if ( e.key == 'P' ) activate("pitch")
    if ( e.key == 'I' ) activate("import")
    if ( e.key == 'E' ) activate("export")
    if ( e.key == 'M' ) activate("solo_freq")
    if ( e.key == 'N' ) activate("notes")
    if ( e.key == 'K' ) activate("solo_time")
    
    // context

    if ( e.key > -1 && e.key < 10 ) {
        copy_setup(e.key)
    }
});

document.addEventListener("keyup",function(e){
    let key = e.key.toLowerCase()
    if(key=="shift"){
      keys[key]=false;
    }else{
    released_keys[key] = true;
    }
});

function activate(id){
    if ( focus_on || context_on ) return
    var el = document.querySelector('#'+id)
    el.click()
    el.classList.add("activated")
    setTimeout(function(){
        el.classList.remove("activated")
    },200)
}

function copy_setup(n){
    if ( focus_on ) return
    var el = document.getElementsByClassName("organ")[n]
    if (!el) return
    document.querySelector("#organs").querySelector("input").value = '['+n+']' 
    document.querySelector("#controls").querySelector("#waveform").querySelector("input").value = el.querySelector("#waveform").querySelector("input").value
    document.querySelector("#controls").querySelector("#frequency").querySelector("input").value = el.querySelector("#frequency").querySelector("input").value
    document.querySelector("#controls").querySelector("#gain").querySelector("input").value = el.querySelector("#gain").querySelector("input").value
    document.querySelector("#controls").querySelector("#duration").querySelector("input").value = el.querySelector("#duration").querySelector("input").value
    document.querySelector("#controls").querySelector("#sample").querySelector("input").value = el.querySelector("#sample").querySelector("input").value
    document.querySelector("#controls").querySelector("#pitch").querySelector("input").value = el.querySelector("#pitch").querySelector("input").value
    el.classList.add("activated")
    setTimeout(function(){
        el.classList.remove("activated")
    },200)
}

function focus(id){
    if ( focus_on ) return
    var context = context_on ? document.getElementsByClassName("organ")[context_on] : document
    var el = context.querySelector('#'+id)
    if (!el) return
    if ( el.focus_on ){
        el.classList.remove("focus");
        el.focus_on = false
        focus_on = undefined
        el.querySelector("input").blur()
    } else {
        el.classList.add("focus");
        el.focus_on = true
        focus_on = id
        setTimeout(function(){
            el.querySelector("input").focus()
        },17)
    }
}


// --- Controls ---

var memory = []

var organs = document.getElementsByClassName("organ")

var controls_article = document.querySelector("#controls")
var controls = controls_article.getElementsByTagName("article")

for ( var i = 2 ; i < controls.length-2 ; i++ ){
    controls[i].addEventListener("click",function(){
        to_interface(this)
    })
}

function to_interface(el){
    var organs_id = eval(document.querySelector("#organs").querySelector("input").value)
    if(el.querySelector("input")) var content = el.querySelector("input").value
    var fragments = []
    if ( !organs_id || !content ) return
    for ( var i = 0 ; i < organs_id.length ; i ++ ) {
        var to = organs[organs_id[i]].querySelector('#'+el.id).querySelector("input")
        fragments.push([to,to.value])
        if(content) to.value = content 
        to.classList.add("activated")
    }
    setTimeout(function(){
        for ( var i = 0 ; i < organs_id.length ; i ++ ) {
            var to = organs[organs_id[i]].querySelector('#'+el.id).querySelector("input")
                to.classList.remove("activated")
        }
    },200)
    memory.push(fragments)
}

document.querySelector("#solo_time").addEventListener("click",function(){
  solo_mode = solo_mode ? false : true;
  console.log(solo_mode ? "start solo" : "stop solo");
})

document.querySelector("#undo").addEventListener("click",function(){
    var undo = memory.pop()
    if ( !undo ) return
    for ( var i = 0 ; i < undo.length ; i ++ ) {
        undo[i][0].value = undo[i][1]
        undo[i][0].classList.add("activated")
    }
    setTimeout(function(){
        for ( var i = 0 ; i < undo.length ; i ++ ) {
            undo[i][0].classList.remove("activated")
        }
    },200)
})

document.querySelector("#apply").addEventListener("click",function(){
    var fragments = []
    var organs_id = eval(document.querySelector("#organs").querySelector("input").value)
    if ( !organs_id ) return
    for ( var j = 0 ; j < organs_id.length ; j ++ ) {
        for ( var i = 4 ; i < controls.length-2 ; i++ ){
            var el = controls[i]
            if ( el.querySelector("input") ) var content = el.querySelector("input").value;
            if ( !content ) continue
            if ( organs[organs_id[j]].querySelector('#'+el.id)){
                var to = organs[organs_id[j]].querySelector('#'+el.id).querySelector("input")
                fragments.push([to,to.value])
                to.value = content 
            }
        }
    }
    memory.push(fragments)
})

document.querySelector("#clear").addEventListener("click",function(){
    var inputs = document.getElementById("controls").getElementsByTagName("input");
    for( var i = 4 ; i < inputs.length ; i++ ){
      inputs[i].value = boot_values[i];
    }; 
    var fragments = []
    var organs_id = eval(document.querySelector("#organs").querySelector("input").value)
    if ( !organs_id ) return
    for ( var j = 0 ; j < organs_id.length ; j ++ ) {
        for ( var i = 4 ; i < controls.length-2 ; i++ ){
            var el = controls[i]
            if ( el.querySelector("input") ) var content = el.querySelector("input").value;
            if ( !content ) continue
            if ( organs[organs_id[j]].querySelector('#'+el.id)){
                var to = organs[organs_id[j]].querySelector('#'+el.id).querySelector("input")
                fragments.push([to,to.value])
                to.value = content 
            }
        }
    }
    memory.push(fragments)
})


// --- Interface ---

// create web audio api context
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioCtx = new AudioContext();

var interfaces = []

Interface = function(){

    // Interface status

    this.changing_frequency = false
    this.changing_gain = false
    this.frequence_events = []
    this.gain_events = []
    this.type = "sine"
    this.duration = [1,0]
    this.tempo_pass = 0

    interfaces.push(this)
}
// define variables



Interface.prototype.setup = function(){
    
    // --- Sample --- 

    // define variables
    
    if ( this.sample != this.past_sample ){

        this.past_sample = this.sample

        request = new XMLHttpRequest();
        
        request.open('GET', this.sample, true);
        
        request.responseType = 'arraybuffer';
        
        var interface_context = this

        request.onload = function() {
          let audioData = request.response;
          audioCtx.decodeAudioData(audioData, function(buffer) {
              interface_context.buffer = buffer;
        
            },
        
            function(e){"Error with decoding audio data" + e.error});
        
        }
        
        request.send();
    }
    
    this.source = audioCtx.createBufferSource();
    this.source.buffer = this.buffer;
    this.source.loop = false;
    this.source.playbackRate.value = this.pitch;

    // create Interface and gain node
  
    this.oscillator = audioCtx.createOscillator();
    this.gainNode = audioCtx.createGain();

    // set options for the oscillator

    //this.oscillator.type = "custom"
    if (!Array.isArray(this.type)){
        
        if ( ["sine","sawtooth","square","triangle"].indexOf(this.type) > -1 ){
            this.oscillator.type = this.type
        }

    }else{
    
    var imag = new Float32Array(this.type);   // sine
    var real = new Float32Array(imag.length);  // cos
    var customWave = audioCtx.createPeriodicWave(real, imag);  // cos,sine
    this.oscillator.setPeriodicWave(customWave);
    
    }
    
    // connect oscillator to gain node to speakers

    this.source.connect(this.gainNode);
    this.oscillator.connect(this.gainNode);
    this.gainNode.connect(audioCtx.destination);
 
    this.oscillator.frequency.value = 0
    this.gainNode.gain.value = 0
}

Interface.prototype.trigger = function() {
    
    this.setup()
   
    this.source.start(0)
    var gain_cumulated_time = 0
    var freq_cumulated_time = 0
    var pre_gain = this.gainNode.gain.value
    var pre_freq = this.oscillator.frequency.value
    
    for ( var i = 0 ; i < this.gain_events.length ; i++ ) {
        
        this.cg(
            this.gain_events[i][0], 
            pre_gain, 
            this.gain_events[i][1], 
            this.gain_events[i][2], 
            gain_cumulated_time
        ) 
        
        gain_cumulated_time += this.gain_events[i][1]
        
        pre_gain = this.gain_events[i][0]
    }

    for ( var i = 0 ; i < this.frequence_events.length ; i++ ) {

        this.cf(
            this.frequence_events[i][0], 
            pre_freq, 
            this.frequence_events[i][1],  
            freq_cumulated_time
        ) 

        freq_cumulated_time += this.frequence_events[i][1]

        pre_freq = this.frequence_events[i][0]
    }

    if ( gain_cumulated_time > freq_cumulated_time ){
        if(!gain_cumulated_time) return
        this.oscillator.start(audioCtx.currentTime);
        this.oscillator.stop(audioCtx.currentTime + gain_cumulated_time)
    }else{
        if(!freq_cumulated_time) return
        this.oscillator.start(audioCtx.currentTime);
        this.oscillator.stop(audioCtx.currentTime + freq_cumulated_time)
    }
};


Interface.prototype.cf = function(new_frequency, pre_freq, length, time){
    this.change_frequency(new_frequency, pre_freq, length, time)
}

Interface.prototype.change_frequency = function(new_frequency, pre_freq, length, time){
    var waveArray = []
    waveArray.push(pre_freq)
    waveArray.push(new_frequency)
    // operation not supported
    this.oscillator.frequency.setValueCurveAtTime(waveArray, audioCtx.currentTime+time, length)
}

Interface.prototype.change_gain = function(new_gain, pre_gain, length, progress, time){
    var waveArray = []
    var exp = 2
    if ( new_gain > pre_gain ){
    for ( var i = 0 ; i < progress ; i++ ){
        if ( pre_gain+(pre_gain + new_gain) / exp > new_gain ) continue
        waveArray.push(pre_gain+(pre_gain + new_gain) / exp )
        exp = exp * 2
    }
        waveArray.reverse()
    }else {
    for ( var i = 0 ; i < progress ; i++ ){
        if ( (pre_gain+(pre_gain + new_gain) / exp)-pre_gain < new_gain ) continue
        waveArray.push((pre_gain+(pre_gain + new_gain) / exp)-pre_gain )
        exp = exp * 2
    }
    }
    waveArray.unshift(pre_gain)
    waveArray.push(new_gain)
    try{
    this.gainNode.gain.setValueCurveAtTime(waveArray, audioCtx.currentTime+time, length)
    }catch(e){}
}

Interface.prototype.cg = function(new_gain, pre_gain, length, progress, time){
    this.change_gain(new_gain, pre_gain,length,progress, time)
}

for ( var i = 0 ; i < 10 ; i++ ){
    new Interface()
}

// --- Time ---

var frames = 0
var frames_per_second = 0
var seconds = 0
var tempo_frames = 0
var tempo = [1,4]
var tempo_index = 0
var record = [];

function time(){

    var date = new Date()
    if( date.getSeconds() != seconds ) {
        frames_per_second = frames
        frames = 0
    }
    
    seconds = date.getSeconds()
    
    frames++
    tempo_frames++

    try{
    if ( tempo_frames >= frames_per_second * tempo[0] ) {
        tempo_index ++
        if ( tempo_index >= tempo[1] ) {
            tempo_index = 0
            for ( var i = 0 ; i < organs.length ; i ++ ){
                interfaces[i].tempo_pass = 0 
            }
        }
        var new_tempo = eval(document.querySelector("#tempo").querySelector("input").value)
        if( new_tempo ) tempo = new_tempo 
        for ( var i = 0 ; i < organs.length ; i ++ ){
            interfaces[i].tempo_pass += 1
            if ( interfaces[i].tempo_pass >= interfaces[i].duration[0] && interfaces[i].duration[1] <= tempo_index ) {
                interfaces[i].tempo_pass = 0 
                trigger(i)
            } else {
              console.log("coucou");
              // duration trick
              var duration = eval( organs[i].querySelector("#duration").querySelector("input").value )
              if ( duration ) interfaces[i].duration = duration
            
            }
        }

        if ( solo_mode){
          record.push(false)
          for ( var E in keys ) {
            if ( !notes[E] || !keys[E] ) continue
            var note = notes[E];
            var organs_id = eval(document.querySelector("#organs").querySelector("input").value)
            if ( !organs_id ) return
            console.log(organs_id)
            for ( var j = 0 ; j < organs_id.length ; j ++ ) {
                    var content = document.getElementById("solo_freq").getElementsByTagName("input")[0].value;
                    var pre_notes = document.getElementById("notes").getElementsByTagName("input")[0].value;
                    const regex = /X/gi;
                    content = content.replace(regex, note);
                    content = eval( content );
                    if ( JSON.parse( pre_notes ) ) notes = JSON.parse( pre_notes );
                    for ( var i in notes ){
                      if ( eval(notes[i]) ) notes[i] = eval(notes[i]);
                      if(window[notes[i]]) notes[i] = window[notes[i]];
                    }
                    if ( !content ) continue;
                    trigger(organs_id[j],true);
                    interfaces[organs_id[j]].frequence_events = content;
                    interfaces[organs_id[j]].trigger();
                    //record[record.length-1]
                    if ( keys["shift"] ) {       
                      if ( released_keys[E] ) {  
                        keys[E] = false;         
                        released_keys[E] = false;
                      }                          
                    }else{                       
                      keys[E] = false;           
                      released_keys[E] = false;  
                    }                            
            }                                    
          }                                      
        }                                        
                                                 
                                                 
        tempo_frames = 0                         
                                                 
    }                                            
        }catch(e){                               
            //console.log(e)                     
        }                                        
                                                 
    setTimeout(function(){                       
        time()                                   
    },0)                                         
}                                                

time()

function trigger(n,partial){
        try{
            // waveform
            var type = eval( organs[n].querySelector("#waveform").querySelector("input").value )
            if ( type ) interfaces[n].type = type
            
            // frequency
            var frequence_events = eval( organs[n].querySelector("#frequency").querySelector("input").value )
            if ( frequence_events ) interfaces[n].frequence_events = frequence_events
            
            // gain
            var gain_events = eval( organs[n].querySelector("#gain").querySelector("input").value )
            if ( gain_events ) interfaces[n].gain_events = gain_events
            
            // duration
            var duration = eval( organs[n].querySelector("#duration").querySelector("input").value )
            if ( duration ) interfaces[n].duration = duration
            
            // sample
            var sample = eval( organs[n].querySelector("#sample").querySelector("input").value )
            if ( sample ) interfaces[n].sample = sample
            
            // pitch
            var pitch = eval( organs[n].querySelector("#pitch").querySelector("input").value )
            if ( pitch ) interfaces[n].pitch = pitch

            // trigger
            if ( interfaces[n].type && interfaces[n].type && interfaces[n].frequence_events.length && interfaces[n].gain_events.length && interfaces[n].duration && interfaces[n].sample && interfaces[n].pitch ) {
                if(!partial)interfaces[n].trigger()
                organs[n].classList.add("activated")
                setTimeout(function(){
                    organs[n].classList.remove("activated")
                },100)
            }
        }catch(e){
            console.log(e)
        }
}

// --- Save / Load ---

var save = {}

function load(){

    fetch("src/data.json", {
        method: 'GET',
    })
    .then(function(response) {
        return response.json()
    })
    .then(function(json) {
        save = json
    })

}
load()

document.querySelector("#export").addEventListener( "click", 
    function(){
        var data = {}
        for ( var i = 0 ; i < organs.length ; i++ ){
            data[i] = {}
            var inputs = organs[i].getElementsByTagName("input")
            for ( var j = 0 ; j < inputs.length ; j++ ){
                if( inputs[j].value ) {
                    data[i][inputs[j].parentNode.id] = inputs[j].value
                }
            }
        }
        if (this.getElementsByTagName("input")[0].value) {
            save[ this.getElementsByTagName("input")[0].value] = data
            fetch("src/write.php", {
                method: 'POST',
                body: JSON.stringify(save)
            })
            .then(function(response) {
                //console.log( response )
                load()
            })
        }
    }
)

document.querySelector("#import").addEventListener( "click", 
    function(){
        var index = this.getElementsByTagName("input")[0].value
            console.log(save)
        if (save[index]) {
            console.log(save[index])
            for ( var i = 0 ; i < organs.length ; i++ ){
                var inputs = organs[i].getElementsByTagName("input")
                for ( var j = 0 ; j < inputs.length ; j++ ){
                    if( save[index][i][inputs[j].parentNode.id] ){
                        inputs[j].value = save[index][i][inputs[j].parentNode.id]
                    }
                }
            }
        }
    }
)
